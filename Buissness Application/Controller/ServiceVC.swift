//
//  ServiceVC.swift
//  Buissness Application
//
//  Created by amr on 3/16/18.
//  Copyright © 2018 amr. All rights reserved.
//

import UIKit

class ServiceVC: UIViewController {
    var service : String = ""
    @IBOutlet weak var serviceTextView: UITextView!
    @IBOutlet weak var serviceImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Service #\(service)"
        serviceTextView.text = "This is Service #\(service)"
        serviceImage.image = UIImage(named: "Image\(service)")
        
        // Do any additional setup after loading the view.
    }

  

 
 

}
